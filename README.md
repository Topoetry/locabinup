# Locabinup

Simple POSIX scripts to fetch and update things that I do not want/am too lazy to package. (mostly for things that do not require a lot of dependencies)

## Install :
```shell
$ git clone https://codeberg.org/Topoetry/locabinup
$ cd locabinup
$ make install
```
## Remove :
```shell 
$ make uninstall
```
